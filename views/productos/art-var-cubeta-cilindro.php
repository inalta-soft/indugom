<?php
// 1. get the content Id (here: an Integer) and sanitize it properly
//$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

// 2. get the content from a flat file (or API, or Database, or ...)
//$data = json_decode(file_get_contents('./posts/'. $id . '.json'));

// 3. return the page
return makePage(); 

function makePage() {
    // 1. get the page
    $pageUrl = "http://inalta.com.ar/Clientes/Indugom/#/art_var_cubeta_cilindro";
    
    // 2. generate the HTML with open graph tags
    $html  = '<!doctype html>'.PHP_EOL;
    $html .= '<html ng-app="indugomApp">'.PHP_EOL;
    $html .= '<head>'.PHP_EOL;
    $html .= '<meta name="author" content="Inalta"/>'.PHP_EOL;
    $html .= '<meta property="og:title" content="Título que yo quiera"/>'.PHP_EOL;
    $html .= '<meta property="og:description" content="Acá iría una descripción"/>'.PHP_EOL;
    $html .= '<meta property="og:image" content="http://inalta.com.ar/Clientes/Indugom/images/art-var-cubeta-cilindro2.jpg"/>'.PHP_EOL;
    $html .= '<meta content="0;url='.$pageUrl.'">'.PHP_EOL;
    $html .= '<title>Indugom SRL - Hidráulicos IHB</title>'.PHP_EOL;
    $html .= '</head>'.PHP_EOL;
    $html .= '<body>'.PHP_EOL;


	$html .= '<section id="highlights">
			<script src="js/jquery.zoom.js"></script>
			<script>
			        $(document).ready(function(){
			          $("#ex1").zoom();    
			        });
			</script>
	
			<ol class="breadcrumb">
			  <li><a >Goma/Goma Metal mierda</a></li>
			  <li><a >Artículos Varios</a></li>
			  <li class="active">Cubeta de Cilindro Hidráulico</li>
			</ol>
	
			<div class="page-header">
			  <h1>Cubeta de Cilindro Hidráulico <small>Artículos Varios</small></h1>
			</div>
	
			<span class="zoom" id="ex1">		
			<img src="images/products/art-var-cubeta-cilindro.jpg" width="70%" />
			</span>
	
			<hr />
	
			<h2>Piezas realizadas a pedido bajo plano. Realizamos cotización de Matricería</h2>
		</section>
		<hr />'.PHP_EOL;


    $html .= '</body>'.PHP_EOL;
    $html .= '</html>';
    // 3. return the page
    echo $html;
}
?>