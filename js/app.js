(function (angular) {
    'use strict';

    var indugomApp = angular.module('indugomApp', ['ngRoute', 'vAccordion']);

    // Routing
    indugomApp.config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'views/home.html',
          controller: 'homeController'
        })
        .when('/contacto', {
          templateUrl: 'views/contact.html',
          controller: 'contactController'
        })
        .when('/art_var_cubeta_cilindro', {
          templateUrl: 'views/productos/art-var-cubeta-cilindro.html',
        })
        .otherwise({
          redirectTo: '/'
        });
    });

    // Controllers
    indugomApp.controller('homeController', function ($scope, $http) {
      $http.get('model/products.json')
        .then(function (response) {
          $scope.categories = response.data.categories;
        });

      $scope.expand = function (productId) {
        if (productId !== $scope.expanded) {
          $scope.expanded = productId;
          $http.get('views/products/' + productId + '.html')
            .then(function (response) {
              $('#expanded-product').html(response.data);
            });
          window.location="#expanded-product";
        }
      };

      $('.carousel').carousel({
        interval: 5300
      });
    });

    indugomApp.controller('contactController', function ($scope, $http) {
      // create a blank object to hold our form information
      // $scope will allow this to pass between controller and view
      $scope.formData = {};
      //$scope.showAlertMessage = false;
      //$scope.alertMessage = '';

      $scope.processForm = function () {
        $http
          .get('contact.php?name=' + $scope.formData.name + '&email=' + $scope.formData.email + '&message=' + $scope.formData.message)
          .then(function (data) {
            $scope.showAlertMessage = true;
            if (data.success) {
              $scope.hasError = true;
              $scope.alertMessage = 'Todos los campos son obligatorios.';
            } else {
              $scope.hasError = false;
              $scope.alertMessage = 'Gracias por ponerse en contacto con nosotros.';
            }
          });
      };
    });

    indugomApp.controller('navigationController', function ($scope, $location) {
      $scope.isCurrentPath = function (path) {
        return $location.path() === path;
      };
    });



})(angular);
