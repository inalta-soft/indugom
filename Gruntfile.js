'use strict';

module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    less: {
      all: {
        compress: true,
        files: {
          'css/app.min.css': 'css/src/app.less'
        }
      }
    },

    csslint: {
      all: {
        src: ['css/**/*.css']
      },
      options: {
        csslintrc: '.csslintrc'
      }
    },

    cssmin: {
      target: {
        files: {
          'css/app.min.css': 'css/app.min.css'
        }
      }
    },

    jshint: {
      all: [
        'Gruntfile.js',
        'js/**/*.js'
      ],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    jsonlint: {
      all: {
        src: ['model/**/*.json']
      }
    },

    watch: {
      js: {
        files: ['Gruntfile.js', 'js/**/*.js'],
        tasks: ['js'],
        options: {
          livereload: true
        }
      },
      css: {
        files: ['css/src/**/*.less'],
        tasks: ['css'],
        options: {
          livereload: true
        }
      },
      json: {
        files: ['model/**/*.json'],
        tasks: ['jsonlint'],
        options: {
          livereload: true
        }
      },
      general: {
        files: ['**/*.html'],
        options: {
          livereload: true
        }
      }
    },

    connect: {
      all: {
        options: {
          hostname: '*',
          port: process.env.PORT || '4567',
          base: '.',
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-csslint');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-jsonlint');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-bump');

  grunt.registerTask('js', ['jshint', 'jsonlint']);
  grunt.registerTask('css', ['less', 'csslint', 'cssmin']);
  grunt.registerTask('build', ['js', 'css']);
  grunt.registerTask('start', ['build', /*'test',*/ 'connect', 'watch']);
  grunt.registerTask('test', [/*'intern'*/]);
  grunt.registerTask('default', ['start']);
};
