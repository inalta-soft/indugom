# Indugom

Responsive, mobile first website for [Indugom](http://indugom.com) built using Bootstrap, Less, Lesshat, Font Awesome, Animate.css, jQuery, Angular and Grunt.

## Getting the environment all set up for development

* Make sure you have Node.js and npm installed: https://nodejs.org/
* Make sure Grunt is installed globally: `npm install -g grunt-cli`
* Move to the root of the project and run `npm install` to fetch all the dependencies
* Right after that, run `grunt` to have the server up and running (with livereload) and also to have Grunt watch your files.

The site will be now live on <http://localhost:4567>.
